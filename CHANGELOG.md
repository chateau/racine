# Version history

>  *Only major changes are listed here, there are always minor improvements as well.

## Version 2.0.1
- Resized a tiny bit playerbar handle.
- Inverted up and down arrows.
- Made border a bit thicker around hovered red button.
- Changed angle of chapter titles.

## Version 2.0.0
- Made custom timeline
- Timeline supports chapters
- Improved icons and theme color scheme
- Added this changelog file and package.json file

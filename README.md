# racine

A very simple website to show a list of videos with *one little trick*.  

The trick is that the videos are in two formats, one long, and the other a short accelerated version of the first one.  

When you open a video, the fast version is opened, you can play it and navigate in it, and at any time, you can click on a button to switch to the normal speed one, just at the position you were.

### Usage

 - clone this repository,
 - create a data folder with the following contents:
    * `data/fast` directory should contain the fast videos,
    * `data/normal` directory should contain the videos in normal speed,
    * `data/vignette` directory should contain the fast videos in a reduced quality format (for the index page),
    * `data/list.js` script that should contain the following content adapted to meet your situation (the path to your videos, the title you want for the page, videos titles):

```javascript
var settings = {

  home: {
    title: "my videos title",
    subtitle: "some subtitle",
    credits: "I did this",
    audio: "index.mp3", // optional
  },

  videosList: [
    {
      title: "Film 01",
      normal: "normal/video1.ext",
      fast: "fast/video1-fast.ext",
      vignette: "vignette/video1-vignette.ext",
      chapitres: {
        672.52: "First chapter",
        2766.04: "Second chapter",
        2930.04: "Third chapter",
      },
    },
    {
      title: "Film 02",
      normal: "normal/video2.ext",
      fast: "fast/video2-fast.ext",
      vignette: "vignette/video2-vignette.ext",
      chapitres: {
        300: "First chapter",
        1400: "Second chapter",
        2600: "Third chapter",
        4800: "Fourth chapter",
      },
    },
  ],

};
```

When all this is done, you're good, just open `index.html` and see the result in your browser.  
When you open a video, use the red circle to pass between the fast and normal versions.

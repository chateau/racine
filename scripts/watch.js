
var watch = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    After start created, the following keys will be created:
    previousPageButton: <lib.button·return>,
    redButton: <lib.button·return>,
    videoFast: <watch.createVideo·return>,
    videoNormal: <watch.createVideo·return>,
    videoCurrent: <watch.createVideo·return>,
  */

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  START/STOP
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  start: function (askedVideo) {
    $app.empty();
    watch.createButtons();
    watch.createVideos(askedVideo);
    watch.detectMouseMove();
    currentPage = "watch";
  },

  stop: function () {
    $(document).off("mousemove");
    home.start();
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY VIDEOS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: create individual video
    ARGUMENTS: (
      !askedVideo <>,
      !type <"fast"|"normal">,
    )
    RETURN: <{
      $el <yquerjObject> « video element »,
      play <function(ø)> « start playing video »,
      pause <function(ø)> « pause playing video »,
    }>
  */
  createVideo: function (askedVideo, type) {

    // CREATE VIDEO OBJECT
    var video = {
      type: type,
      $container: $app.div({ class: "watch-video_container watch-video_container-"+ type, }),
      play: function () { video.playerbar.play(); },
      pause: function () { video.playerbar.pause(); },
      toggle: function () { video.$el[0].paused ? video.playerbar.play() : video.playerbar.pause(); },
      chapters: askedVideo.chapitres,
    };

    // CREATE VIDEO ELEMENT
    video.$el = video.$container.video({
      class: "watch-video watch-video-"+ type,
      controls: false,
      // playsinline: true,
    }).click(function () {
      video.toggle();
    });

    // SET VIDEO SOURCE
    video.$el.source({
      src: "data/"+ askedVideo[type],
    });

    // DETECT FULLSCREEN CHANGE
    video.$el[0].onfullscreenchange = toggleFullScreen;

    // CREATE PLAYERBAR ELEMENT
    video.playerbar = playerbar(video);

    // RETURN VIDEO OBJECT
    return video;

  },

  /**
    DESCRIPTION: create fast and normal video elements, playerbars...
    ARGUMENTS: (
      askedVideo <>
    )
    RETURN: <void>
  */
  createVideos: function (askedVideo) {

    watch.videoFast = watch.createVideo(askedVideo, "fast");
    watch.videoNormal = watch.createVideo(askedVideo, "normal");

    // START PLAYING FAST VIDEO
    watch.videoFast.play();
    watch.videoFast.$container.addClass("current");
    watch.redButton.$el.addClass("fast");

    // set current video
    watch.videoCurrent = watch.videoFast;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DETECT MOUSE IS IDLE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  detectMouseMove: function () {

    var timeout = null;
    $(document).on("mousemove", function() {
      clearTimeout(timeout);
      watch.previousPageButton.$el.show();
      watch.redButton.$el.show();
      watch.videoCurrent.playerbar.$el.show();
      timeout = setTimeout(function () {
        watch.redButton.$el.hide();
        watch.previousPageButton.$el.hide();
        watch.videoCurrent.playerbar.$el.hide()
      }, 2000);
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GO TO INDEX PAGE BUTTON
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  createButtons: function () {

    //
    //                              INDEX PAGE BUTTON

    watch.previousPageButton = lib.button({
      $container: $app,
      class: "watch-previous_page_button",
      title: "revenir à la liste (backspace)",
      icon: "menu.png",
      iconHover: "menu-hover.png",
      click: watch.stop,
    });

    //
    //                              RED BUTTON

    watch.redButton = lib.button({
      $container: $app,
      class: "watch-button",
      click: watch.changeVideo,
      icon: "../play.png",
      title: "(s)",
    });

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CHANGE VIDEO
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  changeVideo: function () {

    //
    //                              GET CURRENT VIDEO

    if (watch.videoCurrent.type == "fast") {
      var currentVideo = watch.videoFast;
      var otherVideo = watch.videoNormal;
      watch.redButton.$el.removeClass("fast");
    }
    else {
      var currentVideo = watch.videoNormal;
      var otherVideo = watch.videoFast;
      watch.redButton.$el.addClass("fast");
    };

    //
    //                              CHANGE VIDEO

    otherVideo.$el[0].currentTime = watch.calculatePositionInTimeline(currentVideo.$el[0], otherVideo.$el[0]);
    otherVideo.$container.addClass("current");
    currentVideo.$container.removeClass("current");
    otherVideo.playerbar.play();
    currentVideo.playerbar.pause();

    watch.videoCurrent = otherVideo;

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CALCULATE POSITION
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  calculatePositionInTimeline: function (currentVideo, otherVideo) {
    return (currentVideo.currentTime/currentVideo.duration) * otherVideo.duration;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

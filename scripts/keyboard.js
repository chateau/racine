
var keyboard = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INITIALIZE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: set keyboard shortcuts
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  initialize: function () {

    document.onkeyup = function (e) {

      var modifiers = {
        shift: e.shiftKey,
        alt: e.altKey,
        ctrl: e.ctrlKey,
      };

      // run shortcut if there's one
      if (keyboard.shortcuts[e.key]) keyboard.shortcuts[e.key](modifiers);

    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  KEYCODES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  shortcuts: {

    // SPACEBAR
    " ": function (modifiers) {
      if (currentPage === "watch") watch.videoCurrent.toggle();
    },

    // ARROWS
    "ArrowRight": function (modifiers) {
      var currentTime = watch.videoCurrent.$el[0].currentTime;
      // IMAGE BY IMAGE WITH SHIFT
      if (modifiers.shift) var newTime = currentTime + (1/25)
      // 1s BY 1s WITH CTRL
      else if (modifiers.ctrl) var newTime = currentTime + 1
      // 10s BY 10s WITHOUT
      else var newTime = currentTime + 10;
      // UPDATE VIDEO TIME
      watch.videoCurrent.playerbar.changeTime(newTime);
    },
    "ArrowLeft": function (modifiers) {
      var currentTime = watch.videoCurrent.$el[0].currentTime;
      // IMAGE BY IMAGE WITH SHIFT
      if (modifiers.shift) var newTime = currentTime - (1/25)
      // 1s BY 1s WITH CTRL
      else if (modifiers.ctrl) var newTime = currentTime - 1
      // 10s BY 10s WITHOUT
      else var newTime = currentTime - 10;
      // UPDATE VIDEO TIME
      watch.videoCurrent.playerbar.changeTime(newTime);
    },
    "ArrowDown": function (modifiers) {
      if (watch.videoCurrent.type == "normal" && watch.videoCurrent.chapters) {
        // FIGURE OUT CURRENT TIME
        var currentTime = watch.videoCurrent.$el[0].currentTime + 0.01;
        // LIST CHAPTERS TIMESTAMPS
        var chaptersTimestampsMaybeNotProperlyOrdered = _.keys(watch.videoCurrent.chapters);
        // ORDER TIMESTAMPS (since it's getting timestamps from an object's keys, we must be sure timestamps are properly ordered)
        var chaptersTimestamps = _.sortBy(chaptersTimestampsMaybeNotProperlyOrdered, function (n) { return +n; });
        // FIGURE OUT NEXT CHAPTER TIMESTAMP
        var nextChapterIndex = _.sortedIndex(chaptersTimestamps, currentTime);
        var nextChapterTimestamp = chaptersTimestamps[nextChapterIndex];
        // UPDATE VIDEO TIME (IF THERE IS A NEXT CHAPTER)
        if (nextChapterTimestamp) watch.videoCurrent.playerbar.changeTime(nextChapterTimestamp);
      };
    },
    "ArrowUp": function (modifiers) {
      if (watch.videoCurrent.type == "normal" && watch.videoCurrent.chapters) {
        // FIGURE OUT CURRENT TIME
        var currentTime = watch.videoCurrent.$el[0].currentTime;
        // LIST CHAPTERS TIMESTAMPS
        var chaptersTimestampsMaybeNotProperlyOrdered = _.keys(watch.videoCurrent.chapters);
        // ORDER TIMESTAMPS (since it's getting timestamps from an object's keys, we must be sure timestamps are properly ordered)
        var chaptersTimestamps = _.sortBy(chaptersTimestampsMaybeNotProperlyOrdered, function (n) { return +n; });
        // FIGURE OUT PREVIOUS CHAPTER TIMESTAMP
        var nextChapterIndex = _.sortedIndex(chaptersTimestamps, currentTime);
        var previousChapterTimestamp = chaptersTimestamps[nextChapterIndex - 1];
        // RESTART CHAPTER IF MORE THAN 10 SECONDS AFTER BEGINING OF CHAPTER
        if ((currentTime - previousChapterTimestamp) > 5) watch.videoCurrent.playerbar.changeTime(previousChapterTimestamp || 0)
        // ELSE GO TO PREVIOUS CHAPTER
        else watch.videoCurrent.playerbar.changeTime(chaptersTimestamps[nextChapterIndex - 2] || 0)
      };
    },

    // CONTROLS
    "s": function (modifiers) {
      watch.changeVideo();
    },
    "f": function (modifiers) {
      toggleFullScreen();
    },
    "t": function (modifiers) {
      watch.videoCurrent.playerbar.getTimestamp();
    },

    "Backspace": function (modifiers) {
      watch.stop();
    },

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};


function playerbar (video) {
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  var playerbar = {
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  LOAD A PROGRAM
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    initializeSlider: function () {

      // // load title and description
      // playerbar.$programTitle.text(videoObject.name);
      // playerbar.$programDescription.text($$.date.moment(new Date(videoObject.originallyPublishedAt)).format("YYYY-MM-DD"));

      var videoDuration = Math.round(video.$el[0].duration);

      // create time slider
      var $sliderContainer = playerbar.$timesliderContainer.empty().div();
      playerbar.timeslider = noUiSlider.create($sliderContainer[0], {
        start: 0,
        connect: [true, false],
        step: 1,
        range: {
          min: 0,
          max: videoDuration,
        },
        // add chapter ticks (in normal video if there are chapters)
        pips: video.type == "normal" && video.chapters ? {
          mode: "values",
          values: _.chain(video.chapters).keys().map(Math.round).value(),
          density: 99,
        } : undefined,
      });

      // add chapter titles (in normal video if there are chapters)
      if (video.type == "normal" && video.chapters) _.each(video.chapters, function (chapterTitle, chapterTimestamp) {
        var $chapterMarker = $sliderContainer.find('.noUi-value[data-value="'+ Math.round(chapterTimestamp) +'"]').prev();
        $chapterMarker.click(function () {
          playerbar.changeTime(chapterTimestamp)
        }).div({
         class: "playerbar-chapter-title",
         text: chapterTitle,
        });
      });

      // detect using the slider
      playerbar.timeslider.on("slide", function (value) { // "update" would also run it when set() is fired
        video.$el[0].currentTime = value;
        playerbar.updateTime();
      });

      // set time texts
      playerbar.updateTime(0, videoDuration);

    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  PLAY/PAUSE
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    play: function () {

      video.$el[0].play();
      playerbar.playpauseButton.$icon.css("background-image", 'url("assets/icon/pause.svg")');
      playerbar.playpauseButton.$iconHover.css("background-image", 'url("assets/icon/pause-hover.svg")');

      // every second update text and sliders
      playerbar.currentlyPlayingInterval = setInterval(function () {

        // update time texts and slider
        playerbar.updateTime(
          Math.round(video.$el[0].currentTime),
          Math.round(video.$el[0].duration)
        );
        playerbar.timeslider.set(Math.round(video.$el[0].currentTime));

        // if end of video, set to pause automatically
        if (video.$el[0].currentTime == video.$el[0].duration) playerbar.pause();

      }, 1000);

      playerbar.currentlyPlaying = true;

    },

    pause: function () {

      video.$el[0].pause();
      playerbar.playpauseButton.$icon.css("background-image", 'url("assets/icon/play.svg")');
      playerbar.playpauseButton.$iconHover.css("background-image", 'url("assets/icon/play-hover.svg")');
      clearInterval(playerbar.currentlyPlayingInterval);
      playerbar.currentlyPlaying = false;

    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  CHANGE TIME
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    changeTime: function (newTime) {

      // make sure newTime is not too low or high
      if (newTime < 0) newTime = 0;
      else if (newTime > video.$el[0].duration) newTime = video.$el[0].duration;

      // set time in video, slider, and text
      video.$el[0].currentTime = newTime;
      playerbar.updateTime(
        Math.round(newTime),
        Math.round(video.$el[0].duration)
      );
      playerbar.timeslider.set(Math.round(video.$el[0].currentTime));

    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  UPDATE TIME
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    updateTime: function (currentTime, duration) {
      if (_.isUndefined(currentTime)) currentTime = video.$el[0].currentTime;
      playerbar.$timeline_passedTime.text(playerbar.makeTimeDisplay(Math.round(currentTime)));
      if (!_.isUndefined(duration)) playerbar.$timeline_duration.text(playerbar.makeTimeDisplay(duration));
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  BUTTONS
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    buttons: function () {

      //
      //                              PLAY/PAUSE BUTTON

      // start buttons container
      playerbar.$buttonsStart = playerbar.$el.div({ class: "playerbar-buttons-start", });

      // play/pause button and program thumbnail
      playerbar.playpauseButton = lib.button({
        $container: playerbar.$buttonsStart,
        class: "playerbar-buttons-playpause",
        icon: "play.svg",
        iconHover: "play-hover.svg",
        title: "play/pause (spacebar)",
        click: function () {
          if (playerbar.currentlyPlaying) playerbar.pause()
          else playerbar.play();
        },
      });

      //
      //                              VOLUME BUTTON

      // end buttons container
      playerbar.$buttonsEnd = playerbar.$el.div({ class: "playerbar-buttons-end", });

      // play/pause button and program thumbnail
      playerbar.soundlevelButton = lib.button({
        $container: playerbar.$buttonsEnd,
        class: "playerbar-buttons-volume",
        icon: "volume.svg",
        iconHover: "volume-hover.svg",
        click: function (e) {
          playerbar.$soundlevelSlider.toggle();
          // do not propagat click event until global radio.$el container, otherwise it will trigger closing of slider just after opening it
          e.stopPropagation();
        },
      });

      playerbar.$soundlevelSlider = playerbar.soundlevelButton.$el.div({
        class: "playerbar-buttons-volume-slider",
      }).click(function (e) {
        // make sure that clicking the slider doesn't close it
        e.stopPropagation();
      });
      playerbar.volumeslider = noUiSlider.create(playerbar.$soundlevelSlider[0], {
        start: 0.8,
        connect: [true, false],
        step: 0.01,
        direction: "rtl",
        orientation: "vertical",
        range: {
          min: 0,
          max: 1,
        },
      });

      playerbar.volumeslider.on("slide", function (value) { // "update" would also run it when set() is fired
        if (video.$el && video.$el.length) video.$el[0].volume = value;
      });

      // hide slider when clicking anywhere
      $app.click(function () {
        if (playerbar.$soundlevelSlider.css("display") !== "none") playerbar.$soundlevelSlider.hide();
      });

      //
      //                              FULLSCREEN BUTTON

      playerbar.fullscreenButton = lib.button({
        $container: playerbar.$buttonsEnd,
        class: "playerbar-buttons-fullscreen",
        icon: $app.hasClass("is-fullscreen") ? "shrink.svg" : "enlarge.svg",
        iconHover: $app.hasClass("is-fullscreen") ? "shrink-hover.svg" : "enlarge-hover.svg",
        title: "fullscreen (f)",
        click: function (e) {
          toggleFullScreen();
        },
      });

      //                              ¬
      //

    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  PROGRAM TITLE
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    // title: function () {
    //
    //   // texts container
    //   playerbar.$titlesContainer = playerbar.$el.div({ class: "playerbar-title_container", });
    //
    //   // title and description/date containers
    //   playerbar.$programTitle = playerbar.$titlesContainer.div({ class: "playerbar-title", });
    //   playerbar.$programDescription = playerbar.$titlesContainer.div({ class: "playerbar-description", });
    //
    // },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  TIMELINE
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    timeline: function () {

      // timeline container
      playerbar.$timeline = playerbar.$el.div({ class: "playerbar-timeline", });

      // timeline passed time
      playerbar.$timeline_passedTime = playerbar.$timeline.div({ class: "playerbar-timeline-passed", });
      // timeline slider
      playerbar.$timesliderContainer = playerbar.$timeline.div({ class: "playerbar-timeline-slider", });
      // timeline duration
      playerbar.$timeline_duration = playerbar.$timeline.div({ class: "playerbar-timeline-duration", });

    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  GENERATE A PROPER DISPLAY FOR THE GIVEN TIME IN SECONDS
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    /**
      DESCRIPTION: convert a time in seconds into a nicely displayed time
      ARGUMENTS: ( !length <integer> « time in seconds » )
      RETURN: <string>
    */
    makeTimeDisplay: function (length) {
      if (!length) length = 0;
      var hours = Math.trunc(length / 3600);
      var minutes = Math.trunc(length / 60) - (hours * 60);
      var seconds = length % 60;

      var resultString = "";

      // hours and minutes
      if (hours) {
        resultString += hours +":";
        resultString += (minutes+"").length < 2 ? "0"+ minutes : minutes;
        resultString += ":";
      }
      // only minutes
      else resultString += minutes +":";

      // add seconds
      resultString += (seconds+"").length < 2 ? "0"+ seconds : seconds;

      return resultString;

    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  GET TIMESTAMP
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    getTimestamp: function () {
      prompt("Timestamp:", Math.round(watch.videoCurrent.$el[0].currentTime * 25)/25);
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  };

  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  playerbar.$el = video.$container.div({ class: "playerbar playerbar_"+ video.type, });
  playerbar.buttons();
  // playerbar.title();
  playerbar.timeline();
  video.$el.on("load")

  // initialize playerbar when video is loaded
  video.$el[0].addEventListener("loadeddata", function (e) {
    playerbar.initializeSlider();
  });

  return playerbar;

  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
};


var lib = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  BUTTON
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: create a clickable button
    ARGUMENTS: ({
      !$container: <yquerjObject>,
      !click: <function(ø)>,
      !icon: <string> « name of an icon present in assets/icon »,
      ?iconHover: <string> « name of a icon present in assets/icon »,
      ?class: <string> « class to apply to button container »,
      ?title: <string> « title bubble »,
    })
    RETURN: <{
      $el: <yquerjObject> « button element container of icon subdivs »,
      $icon: <yquerjObject> « icon element »,
      $iconHover: <yquerjObject> « hovered icon element (if not specified, will be the base icon again) »,
      options: <lib.button·options> « options passed to button creation »,
    }>
  */
  button: function (options) {

    // CREATE BUTTON OBJECT
    var button = {
      options: options,
    };

    // CREATE BUTTON ELEMENT (CONTAINER OF ICONS)
    button.$el = options.$container.div({
      class: "racinelib-button "+ (options.class || ""),
      title: options.title,
    }).click(options.click);

    // CREATE ICON
    button.$icon = button.$el.div({
      class: "racinelib-button-icon",
    }).css("background-image", 'url("assets/icon/'+ options.icon +'"');

    // CREATE HOVERED ICON
    button.$iconHover = button.$el.div({
      class: "racinelib-button-icon_hover",
    }).css("background-image", 'url("assets/icon/'+ (options.iconHover || options.icon) +'"');

    // RETURN BUTTON OBJECT
    return button;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
